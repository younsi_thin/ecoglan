package com.ecoglan.ecoglan.repositories;

import com.ecoglan.ecoglan.models.EcoGleaner;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface EcoGleanerRepository extends MongoRepository<EcoGleaner, Long> {
  @Query(value = "{}")
  public List<EcoGleaner> findAll();

  EcoGleaner findByFirstName(String firstName);

}
