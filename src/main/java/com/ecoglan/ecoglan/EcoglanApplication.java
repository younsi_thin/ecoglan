package com.ecoglan.ecoglan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcoglanApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcoglanApplication.class, args);
    }

}
