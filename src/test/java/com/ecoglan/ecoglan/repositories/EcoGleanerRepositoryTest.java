package com.ecoglan.ecoglan.repositories;

import com.ecoglan.ecoglan.models.EcoGleaner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EcoGleanerRepositoryTest {
    @Autowired
    private EcoGleanerRepository ecoGleanersRepository;

    @Before
    public void setUp() throws Exception{
        EcoGleaner ecoGleaner = new EcoGleaner("Smith", "John","Soat","89 quai Panhard et Levassor",
                                    "75013", "Paris",
                                    "", "0123548795");
        //save product, verify has ID value after save
        assertNull(ecoGleaner.getId());
        this.ecoGleanersRepository.save(ecoGleaner);
        assertNotNull(ecoGleaner.getId());
    }


    @Test
    public void testFetchData() {
        EcoGleaner ecoGleaner = ecoGleanersRepository.findByFirstName("John");
        assertNotNull(ecoGleaner);

        assertEquals("Soat", ecoGleaner.getBusinessName());
        // Get all product, List should only have one
        Iterable<EcoGleaner> ecoGleaners = ecoGleanersRepository.findAll();
        int count = 0;
        for(EcoGleaner eg : ecoGleaners){
            count++;
        }

        assertEquals(count, 1);
    }

    @Test
    public void testDataUpdate(){
        EcoGleaner ecoGleaner = ecoGleanersRepository.findByFirstName("John");
        ecoGleaner.setEmail("toto@hotmail.fr");
        ecoGleanersRepository.save(ecoGleaner);
        assertNotNull(ecoGleaner);
        assertEquals("toto@hotmail.fr", ecoGleaner.getEmail());
    }

    @After
    public void tearDown() throws Exception{
        this.ecoGleanersRepository.deleteAll();
    }
}
